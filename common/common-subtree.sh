#!/bin/sh

usage () {
	echo "usage: $0 [add|pull]"
	echo ""
	echo "'$0 add' adds the common subtree to an apertis-tests repository"
	echo "'$0 pull' is used to update the common subtree in apertis-tests"
}

case $1 in
pull)
git subtree pull -P common git@gitlab.apertis.org:fredo/apertis-tests-common.git master
;;
add)
git subtree add -P common git@gitlab.apertis.org:fredo/apertis-tests-common.git master
;;
*) usage;;
esac
